/**
 * View Models used by Spring MVC REST controllers.
 */
package com.costin.jhipster.web.rest.vm;
