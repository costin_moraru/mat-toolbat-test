import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { trigger,style,transition,animate,keyframes,query,stagger, state } from '@angular/animations';

@Component({
    selector: 'mat-fab-toolbar',
    templateUrl: './mat-fab-toolbar.template.html',
    styleUrls: [
        'mat-fab-toolbar.scss'
    ],
    animations: [
        trigger('showToolbar', [
            state('fab', style({
                position: 'absolute', width: '50px', offset: 0
            })),
            state('expand',   style({
                position: 'absolute', width: '100%', offset: 0, borderRadius: 0
            })),
            transition('fab => expand', animate('200ms ease-out')),
            transition('expand => fab', animate('200ms ease-in'))
          ])
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatFabToolbarComponent implements OnInit {
    @Input() mdOpen: boolean;
    @Input() mdDirection: string;
    @Input() icon: string;
    @Input() color: string = 'red';
    @Output() clicked: EventEmitter<any> = new EventEmitter();
    @Input() disabled: boolean = false;
    @ViewChild('elementref') elementref;
    @ViewChild('contentref') contentref;

    expand = false;

    constructor() { }

    get stateName() {
        return this.expand ? 'fab' : 'expand'
    }


    toggle() {
        this.expand = !this.expand;
    }

    ngOnInit() {
        this.toggle();
    }

    emitClickEvent($event: Event) {
        if (this.disabled)
          return this.disabled;
    
        this.clicked.emit($event);
      }

}
